package com.example.android.marsrealestate.guitar.guitar_list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentGuitarListBinding

class GuitarListFragment : Fragment() {

    lateinit var binding: FragmentGuitarListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = FragmentGuitarListBinding.inflate(inflater)

        return binding.root
    }
}
