package com.example.android.marsrealestate.bicycle

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R


@BindingAdapter("listDataBicycle")
fun bindRecyclerViewBicycle(recyclerView: RecyclerView, data: List<Bicycle>?) {
    val adapter = recyclerView.adapter as BicycleRecyclerAdapter
    adapter.submitList(data)
}



@BindingAdapter("bicycleApiStatus")
fun bindStatusPhoto(statusImageView: ImageView, status: BicycleApiStatus?) {
    when (status) {
        BicycleApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        BicycleApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        BicycleApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}