package com.example.android.marsrealestate.tablet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Tablet (
        val id: Long,
        val model: String,
        val typeOfProduct: String,
        val manufacturer: String,
        val manufacturerCountry: String,
        val url: String,
        val productionYear: Integer,
        val screenDiagonal: String,
        val screenResolution: String,
        val ram: String,
        val hasSim: Boolean,
        val cameraResolution: String,
        val seller: String
        // val productIsNew: Boolean
) : Parcelable