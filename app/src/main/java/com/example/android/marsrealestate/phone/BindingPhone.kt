package com.example.android.marsrealestate.phone

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.example.android.marsrealestate.lathe.LatheApiStatus
import com.example.android.marsrealestate.phone.model.Phone


@BindingAdapter("listDataPhones")
fun bindRecyclerViewPhones(recyclerView: RecyclerView, data: List<Phone>?) {
    val adapter = recyclerView.adapter as PhoneRecyclerAdapter
    adapter.submitList(data)
}



@BindingAdapter("phoneApiStatus")
fun bindStatusPhoto(statusImageView: ImageView, status: PhoneApiStatus?) {
    when (status) {
        PhoneApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        PhoneApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        PhoneApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}