package com.example.android.marsrealestate.guitar.guitar_detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.android.marsrealestate.guitar.Guitar

class GuitarDetailViewModel(guitar: Guitar, app: Application) : AndroidViewModel(app) {
    private val _currentGuitar = MutableLiveData<Guitar>()
    val currentGuitar: LiveData<Guitar>
        get() = _currentGuitar

    init {
        _currentGuitar.value = guitar
    }
}