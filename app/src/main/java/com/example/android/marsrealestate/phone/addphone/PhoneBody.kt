package com.example.android.marsrealestate.phone.addphone

import com.google.gson.annotations.SerializedName

data class PhoneBody(

        @SerializedName("model")
        var model: String = "",

        @SerializedName("manufacturer")
        var manufacturer: String = "",

        @SerializedName("country")
        var country: String = "",

        @SerializedName("has3g")
        var has3g: Boolean = false,

        @SerializedName("has4g")
        var has4g: Boolean = false,

        @SerializedName("url")
        var url: String = "https://cdn.mos.cms.futurecdn.net/J8DirbjgmENqft2LGkuMBZ-650-80.jpg"
)
