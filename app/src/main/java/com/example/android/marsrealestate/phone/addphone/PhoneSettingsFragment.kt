package com.example.android.marsrealestate.phone.addphone

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentPhoneSettingsBinding


//class PhoneSettingsFragment : Fragment(R.layout.fragment_phone_settings) {
class PhoneSettingsFragment : Fragment() {

    private val TAG : String = "PhoneSettingsFragment"

    lateinit var binding: FragmentPhoneSettingsBinding

    private val viewModel: PhoneSettingsViewModel by lazy {
        ViewModelProviders.of(this).get(PhoneSettingsViewModel::class.java)
    }

    // for saving data
    private lateinit var phoneInfoViewModel: PhoneInfoViewModel



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application

        binding = FragmentPhoneSettingsBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel


        activity?.let {
            phoneInfoViewModel = ViewModelProviders.of(it).get(PhoneInfoViewModel::class.java)

            viewModel.is3GSelected.value = phoneInfoViewModel.isPhoneHas3G.get()
            viewModel.is4GSelected.value = phoneInfoViewModel.isPhoneHas4G.get()

            viewModel.is3GSelected.observe(viewLifecycleOwner, Observer {
                phoneInfoViewModel.isPhoneHas3G.set(it)
                Log.d(TAG, "dm-> 3G is $it ")
            })

            viewModel.is4GSelected.observe(viewLifecycleOwner, Observer {
                phoneInfoViewModel.isPhoneHas4G.set(it)
                Log.d(TAG, "dm-> 4G is $it ")
            })
        }



        return binding.root
    }

}