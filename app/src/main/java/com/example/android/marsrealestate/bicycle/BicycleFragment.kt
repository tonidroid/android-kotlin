package com.example.android.marsrealestate.bicycle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentBicycleListBinding

class BicycleFragment: Fragment() {

    private val viewModel: BicycleViewModel by lazy {
        ViewModelProviders.of(this).get(BicycleViewModel::class.java)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val binding = FragmentBicycleListBinding.inflate(inflater)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel


        binding.bicycleRecycler.adapter = BicycleRecyclerAdapter(BicycleRecyclerAdapter.OnClickListenerBicycle{
            viewModel.displayPropertyDetails(it)
        })


        viewModel.navigateToSelectedBicycle.observe(this, Observer {
            it?.let {
                this.findNavController()
                        .navigate(BicycleFragmentDirections.actionBicycleFragmentToBicycleDetailFragment(it))
                viewModel.displayPropertyDetailsComplete()
            }
        })
        return binding.root
    }

   
}