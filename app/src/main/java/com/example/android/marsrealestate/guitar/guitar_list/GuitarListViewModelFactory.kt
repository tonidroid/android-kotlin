package com.example.android.marsrealestate.guitar.guitar_list

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.android.marsrealestate.guitar.Guitar

class GuitarListViewModelFactory(
        private val guitar: Guitar,
        private val app: Application
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GuitarListViewModel::class.java)) {
            return GuitarListViewModel(guitar, app) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class!")
    }
}