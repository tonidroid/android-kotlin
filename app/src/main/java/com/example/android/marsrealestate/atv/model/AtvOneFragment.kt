package com.example.android.marsrealestate.atv.model

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentAtvDetailBinding

class AtvOneFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application
        val binding =  FragmentAtvDetailBinding.inflate(inflater)
        binding.lifecycleOwner = this

        val atvProperty = AtvOneFragmentArgs.fromBundle(arguments!!).selectedAtvProperty
        val viewModelFactory = AtvOneViewModelFactory(atvProperty, application)
        binding.viewModel = ViewModelProviders.of(
                this, viewModelFactory).get(AtvOneViewModel::class.java)

        return binding.root
    }
}