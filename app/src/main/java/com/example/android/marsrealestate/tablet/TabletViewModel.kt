package com.example.android.marsrealestate.tablet

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.tablet.model.Tablet
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

enum class TabletApiStatus {
    LOADING, ERROR, DONE
}

class TabletViewModel : ViewModel() {


    val tablet: LiveData<List<Tablet>>
        get() = _tablet
    private val _tablet = MutableLiveData<List<Tablet>>()

    val status: LiveData<TabletApiStatus>
        get() = _status
    private val _status = MutableLiveData<TabletApiStatus>()


    val navigateToSelectedTablet: LiveData<Tablet> get() = _navigateToSelectedTablet
    private val _navigateToSelectedTablet = MutableLiveData<Tablet>()


    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    init {
        getTabletProperties()
    }

    private fun getTabletProperties() {

        coroutineScope.launch {
            var getPropertiesDeferred = TabletApi.tabletApiService.getTabletList()

            try {
                _status.value = TabletApiStatus.LOADING
                var listResult = getPropertiesDeferred.await()
                _status.value = TabletApiStatus.DONE
                _tablet.value = listResult
            } catch (e: Exception) {
                _status.value = TabletApiStatus.ERROR
                Log.d("Error dm - > ", "Internet connection not exist or Server is shot down")
                _tablet.value = ArrayList()
            }

        }

    }


    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel() // !! Important
    }

    fun displayOneLathe(tablet: Tablet) {
        _navigateToSelectedTablet.value = tablet
    }

    fun displayOneLatheComplete() {
        _navigateToSelectedTablet.value = null
    }


}