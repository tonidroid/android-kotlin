package com.example.android.marsrealestate.phone.addphone

import androidx.databinding.ObservableField


interface PhoneNavigator{
    fun showToast(model: String, country: String)
    fun errorMessages(isModelFilled : Boolean, isCountryFilled: Boolean)

    fun get3G() : ObservableField<Boolean>
    fun get4G() : ObservableField<Boolean>

    fun createSelectManufacturerDialog()
}