package com.example.android.marsrealestate.atv

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.example.android.marsrealestate.atv.model.ATV

@BindingAdapter("listDataAtv")
fun bindRecyclerViewAtv(recyclerView: RecyclerView, data: List<ATV>?) {
    val adapter = recyclerView.adapter as AtvRecyclerAdapter
    adapter.submitList(data)
}



@BindingAdapter("atvApiStatus")
fun bindStatusAtv(statusImageView: ImageView, status: AtvApiStatus?) {
    when (status) {
        AtvApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        AtvApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        AtvApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}