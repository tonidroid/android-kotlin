package com.example.android.marsrealestate.atv.model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class AtvOneViewModel(atv: ATV, app: Application) : AndroidViewModel(app) {

    val selectedProperty: LiveData<ATV> get() = _selectedProperty
    private val _selectedProperty = MutableLiveData<ATV>()


    init {
        _selectedProperty.value = atv
    }
}
