package com.example.android.marsrealestate.phone.addphone

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.RetrofitApi
import com.example.android.marsrealestate.phone.model.PhoneResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference


class PhoneNewViewModel : ViewModel() {

    private val TAG = "PhoneNewViewModel"

    var modelPhone = ObservableField<String>("") // for field
    var countryPhone = ObservableField<String>("") // for field
    var brandPhone = ObservableField<String>("") // for field
//    var brandObservable = MutableLiveData<String>()

    var navigateToPhoneCharacteristics = MutableLiveData<Boolean>(false)

    private var manufacturerType: PhoneManufacturerType? = null

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    fun onSendButton() {

        if (allFilldsIGood()) {
            val model = modelPhone.get() ?: ""
            val manufacturer = brandPhone.get() ?: ""
            val country = countryPhone.get() ?: ""
            val has3g = getNavigator()?.get3G()?.get() ?: false
            val has4g = getNavigator()?.get4G()?.get() ?: false

            Log.d(TAG, "dm-> has3g = $has3g, has4g = $has4g")
            Log.d(TAG, "dm-> model = $model, country = $country")

            savePhone(model, manufacturer, country, has3G = has3g, has4G = has4g)

            getNavigator()?.showToast(model = "Phone $model is saved!", country = country)  // for easier understanding this code can be deleted

            modelPhone.set("")
            countryPhone.set("")
            brandPhone.set("")

        } else {
            getNavigator()?.showToast(model = "Phone not Save !!", country = "")
        }


    }

    private fun savePhone(model: String, manufacturer: String, country: String, has3G: Boolean, has4G: Boolean) {

        Log.d("LatheViewModel dm->", "dm->- saving....")
//        savePhoneWithDeferred(model, country, has3G = has3G, has4G = has4G) // 1
        savePhoneWithCall(model, manufacturer, country,   has3G = has3G, has4G = has4G) // 2

    }


    private fun savePhoneWithCall(model: String, manufacturer: String, country: String, has3G: Boolean, has4G: Boolean) {

        RetrofitApi.retrofitApiServiceCall
                .savePhone(PhoneBody(model, manufacturer, country, has3G, has4G)).enqueue(object : Callback<PhoneResponse> {
                    override fun onResponse(call: Call<PhoneResponse>, response: Response<PhoneResponse>) {
//                        val respons = response.body().toString()
                        val respons = response.code()
                        Log.d(TAG, "dm-> saved  object: $respons")
                    }

                    override fun onFailure(call: Call<PhoneResponse>, t: Throwable) {
                        Log.d(TAG, "dm-> Something wrong: ${t.message}")
                    }

                })
    }

    private fun savePhoneWithDeferred(model: String, manufacturer: String, country: String, has3G: Boolean, has4G: Boolean) {

        coroutineScope.launch {
            //                var getPropertiesDeferred = LatheApi.retrofitService.getProperties( )
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.savePhone(PhoneBody(model, manufacturer, country, has3G, has4G))
            try {
//                    _status.value = LatheApiStatus.LOADING
                Log.d(TAG, "dm-> all good")
                var phoneResponse: PhoneResponse = getPropertiesDeferred.await()
                Log.d(TAG, "dm-> $phoneResponse")
//                    _status.value = LatheApiStatus.DONE
//                _properties.value = listResult
            } catch (e: Exception) {
//                _status.value = LatheApiStatus.ERROR
                Log.d(TAG, "dm-> Error :: ${e.message}")
//                _properties.value = ArrayList()
            }
        }
    }


    fun allFilldsIGood(): Boolean {
        var isModelFilled = false
        var isCountryFilled = false
        var isBrandFilled = false

        if (!modelPhone.get().isNullOrEmpty()) isModelFilled = true
        if (!countryPhone.get().isNullOrEmpty()) isCountryFilled = true
        if (!brandPhone.get().isNullOrEmpty()) isBrandFilled = true

        Log.d(TAG, "dm-> brand: ${brandPhone.get()}")

        if (!isModelFilled || !isCountryFilled || !isBrandFilled) {
            getNavigator()?.errorMessages(isModelFilled, isCountryFilled)
            return false
        } else {
            return true
        }

    }

    fun updateManufacturer(selectedIndex:  Int){
        manufacturerType = when(selectedIndex.toString()){
            PhoneManufacturerType.APPLE.id -> PhoneManufacturerType.APPLE
            PhoneManufacturerType.XIOMI.id -> PhoneManufacturerType.XIOMI
            PhoneManufacturerType.HTC.id -> PhoneManufacturerType.HTC
            else -> PhoneManufacturerType.OTHER
        }
//        brandObservable.value = manufacturerType?.getBrand() ?: ""
        brandPhone.set(manufacturerType?.getBrand() ?: "")
    }

    fun onPhoneManufacturerClick() {
        getNavigator()?.createSelectManufacturerDialog()
    }

    fun onCharacteristicsClick() {
        displaySetting()
//        getNavigator()?.onPhoneCharacteristicsClick()
    }


    fun displaySetting() {
        navigateToPhoneCharacteristics.value = true
    }

    fun displaySettingComplete() {
        navigateToPhoneCharacteristics.value = false
    }


    // Navigator
    private var mNavigator: WeakReference<PhoneNavigator>? = null  // for easier understanding this code can be deleted

    fun getNavigator() = mNavigator?.get()  // for easier understanding this code can be deleted

    fun setNavigator(navigator: PhoneNavigator) {  // for easier understanding this code can be deleted
        clearNavigator()
        mNavigator = WeakReference<PhoneNavigator>(navigator)
    }

    private fun clearNavigator() = mNavigator?.clear()  // for easier understanding this code can be deleted
}

