package com.example.android.marsrealestate.guitar.guitar_detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders

import com.akordirect.vmccnc.databinding.FragmentGuitarDetailBinding
import com.example.android.marsrealestate.guitar.Guitar

//фрагмент, предназначенный для вывода информации об одной выбранной гитаре
class GuitarDetailFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentGuitarDetailBinding.inflate(inflater)
        binding.lifecycleOwner = this

        //val guitar = GuitarDetailFragmentArgs.fromBundle(arguments!!).currentGuitar
        val guitar = Guitar("mod", "typ", "nec",
                "bod", 6, 12, "col", "description")
        val viewModelFactory = GuitarDetailViewModelFactory(
                guitar, requireNotNull(activity).application)

        //инициализация данных
        binding.data = ViewModelProviders.of(this, viewModelFactory)
                .get(GuitarDetailViewModel::class.java)

        return binding.root
    }
}
