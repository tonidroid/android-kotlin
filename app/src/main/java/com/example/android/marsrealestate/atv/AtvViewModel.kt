package com.example.android.marsrealestate.atv

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.atv.model.ATV
import com.example.android.marsrealestate.tablet.TabletApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

enum class AtvApiStatus {
    LOADING, ERROR, DONE
}

class AtvViewModel : ViewModel() {


    val atv: LiveData<List<ATV>>
        get() = _atv
    private val _atv = MutableLiveData<List<ATV>>()

    val status: LiveData<AtvApiStatus>
        get() = _status
    private val _status = MutableLiveData<AtvApiStatus>()


    val navigateToSelectedAtv: LiveData<ATV> get() = _navigateToSelectedAtv
    private val _navigateToSelectedAtv = MutableLiveData<ATV>()


    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    init {
        getAtvProperties()
    }

    private fun getAtvProperties() {

        coroutineScope.launch {
            var getPropertiesDeferred = TabletApi.tabletApiService.getAtvList()

            try {
                _status.value = AtvApiStatus.LOADING
                var listResult = getPropertiesDeferred.await()
                _status.value = AtvApiStatus.DONE
                _atv.value = listResult
            } catch (e: Exception) {
                _status.value = AtvApiStatus.ERROR
                Log.d("Error dm - > ", "Internet connection not exist or Server is shot down")
                _atv.value = ArrayList()
            }

        }

    }


    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel() // !! Important
    }

    fun displayOneAtv(atv: ATV) {
        _navigateToSelectedAtv.value = atv
    }

    fun displayOneAtvComplete() {
        _navigateToSelectedAtv.value = null
    }

    interface RetrofitApiService{}


}