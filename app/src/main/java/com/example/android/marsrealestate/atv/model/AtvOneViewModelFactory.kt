package com.example.android.marsrealestate.atv.model

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AtvOneViewModelFactory (private val tablet: ATV, private val application: Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AtvOneViewModel::class.java)) {
            return AtvOneViewModel(tablet, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}