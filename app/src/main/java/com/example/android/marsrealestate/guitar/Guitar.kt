package com.example.android.marsrealestate.guitar

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Guitar(
        var model: String = "",       //название модели
        var type: String = "",        //тип
        var neck: String = "",        //материал грифа
        var body: String = "",        //тип корпуса
        var scale: Int = 0,           //мензура
        var fretsNumber: Int = 0,     //количество ладов
        var color: String = "",       //цвет
        var description: String = "" //описание
        //@Json(name = "video1") val img: String //изображение
) : Parcelable {
    fun outputInfo() =
            "Модель: $model;\n" +
            "Тип: $type;\n" +
            "Материал грифа: $neck;\n" +
            "Тип корпуса: $body;\n" +
            "Мензура: $scale;\n" +
            "Количество ладов: $fretsNumber;\n" +
            "Цвет: $color;\n" +
            "Описание: $description"

    fun getScale() = scale.toString()
    fun getFretsNumber() = fretsNumber.toString()
}