package com.example.android.marsrealestate.atv.addatv

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentAtvNewBinding
import com.example.android.marsrealestate.tablet.addtablet.TabletNavigator

class AtvNewFragment: Fragment(), TabletNavigator {
    lateinit var binding: FragmentAtvNewBinding
    private val viewModel: AtvNewViewModel by lazy {
        ViewModelProviders.of(this).get(AtvNewViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = FragmentAtvNewBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewModel.setNavigator(this) // for easier understanding this code can be deleted


        return binding.root
    }








    override fun showToast(model: String, country: String) {  // for easier understanding this code can be deleted
        Toast.makeText(context, "model: $model, country: $country", Toast.LENGTH_SHORT).show()
    }

    override fun errorMessages(isModelFilled: Boolean, isManufacturerFilled: Boolean, isCountryFilled: Boolean, isMileageFilled: Boolean, isPowerSupplyFilled: Boolean, isFirstRegistrationFilled: Boolean, isGearBoxFilled: Boolean, isCubicCapacityFilled: Boolean, isCameraResolutionFilled: Boolean) {

        if (!isModelFilled) {
            binding.atvModel.isErrorEnabled = true
            binding.atvModel.error = "Enter model"
        } else {
            binding.atvModel.isErrorEnabled = false
        }

        if (!isManufacturerFilled) {
            binding.atvManufacturer.isErrorEnabled = true
            binding.atvManufacturer.error = "Enter manufacturer"
        } else {
            binding.atvManufacturer.isErrorEnabled = false
        }

        if (!isCountryFilled) {
            binding.atvCountry.isErrorEnabled = true
            binding.atvCountry.error = "Enter country"
        } else {
            binding.atvCountry.isErrorEnabled = false
        }

        if (!isMileageFilled) {
            binding.atvMileage.isErrorEnabled = true
            binding.atvMileage.error = "Enter mileage"
        } else {
            binding.atvMileage.isErrorEnabled = false
        }

        if (!isPowerSupplyFilled) {
            binding.atvPower.isErrorEnabled = true
            binding.atvPower.error = "Enter Power supply"
        } else {
            binding.atvPower.isErrorEnabled = false
        }

        if (!isFirstRegistrationFilled) {
            binding.atvFirstRegistration.isErrorEnabled = true
            binding.atvFirstRegistration.error = "Enter First Registration"
        } else {
            binding.atvFirstRegistration.isErrorEnabled = false
        }

        if (!isGearBoxFilled) {
            binding.atvGearBox.isErrorEnabled = true
            binding.atvGearBox.error = "Enter Gear Box"
        } else {
            binding.atvGearBox.isErrorEnabled = false
        }

        if (!isCubicCapacityFilled) {
            binding.atvCubic.isErrorEnabled = true
            binding.atvCubic.error = "Enter cubic"
        } else {
            binding.atvCubic.isErrorEnabled = false
        }

    }
}