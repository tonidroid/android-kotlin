package com.example.android.marsrealestate.atv.addatv

class AtvBody (
        var model : String,
        var manufacturer: String,
        var manufacturerCountry: String,
        var mileage: String,
        var powerSupply : String,
        var firstRegistration: String,
        var gearBox: String,
        var cubicCapacity: String,
        var productIsNew: Boolean
)