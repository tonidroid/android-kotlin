package com.example.android.marsrealestate.tablet.model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class TabletOneViewModel(tablet: Tablet, app: Application) : AndroidViewModel(app) {

    val selectedProperty: LiveData<Tablet> get() = _selectedProperty
    private val _selectedProperty = MutableLiveData<Tablet>()


    init {
        _selectedProperty.value = tablet
    }


}