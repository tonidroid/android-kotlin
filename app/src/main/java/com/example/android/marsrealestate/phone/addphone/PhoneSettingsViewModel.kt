package com.example.android.marsrealestate.phone.addphone

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PhoneSettingsViewModel :  ViewModel(){

    var is3GSelected = MutableLiveData<Boolean>(false)
    var is4GSelected = MutableLiveData<Boolean>(false)

}
