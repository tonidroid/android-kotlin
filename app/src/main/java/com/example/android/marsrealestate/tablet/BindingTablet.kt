package com.example.android.marsrealestate.tablet

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.example.android.marsrealestate.tablet.model.Tablet


@BindingAdapter("listDataTablets")
fun bindRecyclerViewTablets(recyclerView: RecyclerView, data: List<Tablet>?) {
    val adapter = recyclerView.adapter as TabletRecyclerAdapter
    adapter.submitList(data)
}



@BindingAdapter("tabletApiStatus")
fun bindStatusTablet(statusImageView: ImageView, status: TabletApiStatus?) {
    when (status) {
        TabletApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        TabletApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        TabletApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}