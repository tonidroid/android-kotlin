package com.example.android.marsrealestate.atv.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class ATV (
        val id: Long,
        val model: String,
        val manufacturer: String,
        val url: String,
        val typeOfProduct: String,
        val manufacturerCountry: String,
        val mileage: Integer,
        val cubicCapacity: Integer,
        val powerSupply: String,
        val firstRegistration: String,
        val gearBox: String
        // val productIsNew: Boolean
) : Parcelable


