package com.example.android.marsrealestate.phone.addphone

enum class PhoneManufacturerType (val id: String){
    APPLE("1"){ override fun getBrand(): String = "Apple" },
    XIOMI("2"){ override fun getBrand(): String = "Xiomi" },
    HTC("3"){ override fun getBrand(): String = "HTC" },
    OTHER("4"){ override fun getBrand(): String = "Other" };

    abstract fun getBrand(): String
}