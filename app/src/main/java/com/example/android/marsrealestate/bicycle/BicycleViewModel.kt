package com.example.android.marsrealestate.bicycle

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.RetrofitApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class BicycleViewModel : ViewModel(){

    //status
    private val _status = MutableLiveData<BicycleApiStatus>()

    val statusBicycle: LiveData<BicycleApiStatus>
        get() = _status

    //navigate
    private val _navigateToSelectedBicycle = MutableLiveData<Bicycle>()

    val navigateToSelectedBicycle: LiveData<Bicycle>
        get() = _navigateToSelectedBicycle

    //Bicycle
    val bicycle: LiveData<List<Bicycle>>
        get() = _bicycle
    private val _bicycle = MutableLiveData<List<Bicycle>>()


    private var viewModelJob = Job()

    //coroutine
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main )

    init {
        getBicycleRealEstateProperties()
    }

    private fun getBicycleRealEstateProperties(){

        coroutineScope.launch {
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.getBicycleList()
            try {
                _status.value = BicycleApiStatus.LOADING
                var listResult = getPropertiesDeferred.await()
                _status.value = BicycleApiStatus.DONE
                _bicycle.value = listResult
            } catch (e: Exception) {
                _status.value = BicycleApiStatus.ERROR
                Log.d("Error", "Internet connection not exist or Server is shot down")
                _bicycle.value = ArrayList()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun displayPropertyDetails(bicycleProperty: Bicycle) {
        _navigateToSelectedBicycle.value = bicycleProperty
    }

    fun displayPropertyDetailsComplete() {
        _navigateToSelectedBicycle.value = null
    }

    interface RetrofitApiService{}
}