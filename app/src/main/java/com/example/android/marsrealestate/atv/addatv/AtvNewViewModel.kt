package com.example.android.marsrealestate.atv.addatv

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.tablet.TabletApi
import com.example.android.marsrealestate.tablet.addtablet.TabletNavigator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference

class AtvNewViewModel: ViewModel() {

    private val TAG = "AtvNewViewModel"

    var modelAtv = ObservableField<String>("") // for field
    var manufacturerAtv = ObservableField<String>("") // for field
    var countryAtv = ObservableField<String>("") // for field
    var cubicAtv = ObservableField<String>("") // for field
    var mileageAtv = ObservableField<String>("") // for field
    var powerSupplyAtv = ObservableField<String>("") // for field
    var firstRegistrationAtv = ObservableField<String>("") // for field
    var gearBoxAtv = ObservableField<String>("") // for field
    var productIsNew = ObservableBoolean()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    fun onSendButton() {
        val tr = allFilldsIGood()
        if (tr) {
            val model = modelAtv.get() ?: ""
            val manufacturer = manufacturerAtv.get() ?: ""
            val country = countryAtv.get() ?: ""
            val cubic = cubicAtv.get() ?: ""
            val mileage = mileageAtv.get() ?: ""
            val powerSupply = powerSupplyAtv.get() ?: ""
            val firstRegistration = firstRegistrationAtv.get() ?: ""
            val gearBox = gearBoxAtv.get() ?: ""
            val productIsNew = productIsNew.get()

            Log.d(TAG, "dm-> model =$model, country = $country + $cubic")

            getNavigator()?.showToast(model = model, country = country)  // for easier understanding this code can be deleted

            saveAtv(model, manufacturer, country, cubic, mileage, powerSupply, firstRegistration, gearBox, productIsNew)
        }else{
            getNavigator()?.showToast(model = "ATV not Save !!", country = "")
        }
    }

    private fun saveAtv(model: String, manufacturer: String, manufacturerCountry: String, cubicCapacity: String, mileage: String, powerSupply: String, firstRegistration: String, gearBox: String, productIsNew: Boolean) {

        Log.d("LatheViewModel dm->", "- getLatheProperties")
//            _response.value = "Set response Lathe"

        coroutineScope.launch {
            //                var getPropertiesDeferred = LatheApi.retrofitService.getProperties( )
            var getPropertiesDeferred = TabletApi.tabletApiService.saveAtv(AtvBody(model, manufacturer, manufacturerCountry, mileage, powerSupply, firstRegistration, gearBox, cubicCapacity, productIsNew))
            try {
//                    _status.value = LatheApiStatus.LOADING
                Log.d("LatheViewModel", "dm-> all good")
                var listResult = getPropertiesDeferred.await()
//                    _status.value = LatheApiStatus.DONE
//                _properties.value = listResult
            } catch (e: Exception) {
//                _status.value = LatheApiStatus.ERROR
                Log.d("LatheViewModel", "dm-> I think and shure you do not have and internet connection")
//                _properties.value = ArrayList()
            }
        }


    }

    fun allFilldsIGood(): Boolean {
        var isModelFilled = false
        var isManufacturerFilled = false
        var isCountryFilled = false
        var isMileageFilled = false
        var isPowerSupplyFilled = false
        var isFirstRegistrationFilled = false
        var isGearBoxFilled = false
        var isCubicCapacityFilled = false

        if (!modelAtv.get().isNullOrEmpty()) isModelFilled = true
        if (!manufacturerAtv.get().isNullOrEmpty()) isManufacturerFilled = true
        if (!countryAtv.get().isNullOrEmpty()) isCountryFilled = true
        if (!mileageAtv.get().isNullOrEmpty()) isMileageFilled = true
        if (!powerSupplyAtv.get().isNullOrEmpty()) isPowerSupplyFilled = true
        if (!firstRegistrationAtv.get().isNullOrEmpty()) isFirstRegistrationFilled = true
        if (!gearBoxAtv.get().isNullOrEmpty()) isGearBoxFilled = true
        if (!cubicAtv.get().isNullOrEmpty()) isCubicCapacityFilled = true

        if (!isModelFilled || !isManufacturerFilled || !isCountryFilled || !isMileageFilled || !isPowerSupplyFilled || !isFirstRegistrationFilled || !isGearBoxFilled || !isCubicCapacityFilled) {
            getNavigator()?.errorMessages(isModelFilled, isManufacturerFilled, isCountryFilled,isMileageFilled,isPowerSupplyFilled,isFirstRegistrationFilled,isGearBoxFilled,isCubicCapacityFilled,true)
            return false
        } else {
            return true
        }

    }


    private var mNavigator: WeakReference<TabletNavigator>? = null  // for easier understanding this code can be deleted

    fun getNavigator() = mNavigator?.get()  // for easier understanding this code can be deleted

    fun setNavigator(navigator: TabletNavigator) {  // for easier understanding this code can be deleted
        clearNavigator()
        mNavigator = WeakReference<TabletNavigator>(navigator)
    }

    private fun clearNavigator() = mNavigator?.clear()  // for easier understanding this code can be deleted
}