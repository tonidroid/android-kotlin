package com.example.android.marsrealestate.phone

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentPhoneBinding


class PhoneFragment : Fragment() {

    private val viewModel: PhoneViewModel by lazy {
        ViewModelProviders.of(this).get(PhoneViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = FragmentPhoneBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        binding.lasersGrid.adapter = PhoneRecyclerAdapter(PhoneRecyclerAdapter.OnClickListenerPhone {
            Toast.makeText(context, "I will redirect to One Phone", Toast.LENGTH_SHORT).show()
            Log.d("LaserListFragment", " dm-> It will redirect to to One Phone")
        })

        return binding.root
    }


}
