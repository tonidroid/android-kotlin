package com.example.android.marsrealestate.tablet.addtablet

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.tablet.TabletApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference

class TabletNewViewModel : ViewModel() {

    private val TAG = "TabletNewViewModel"

    var modelTablet = ObservableField<String>("") // for field
    var countryTablet = ObservableField<String>("") // for field
    var manufacturer = ObservableField<String>("") // for field
    var productionYear = ObservableField<String>("") // for field
    var screenDiagonal = ObservableField<String>("") // for field
    var screenResolution = ObservableField<String>("") // for field
    var ram = ObservableField<String>("") // for field
    var cameraResolution = ObservableField<String>("") // for field
    var seller = ObservableField<String>("") // for field
    var sim = ObservableBoolean()
    var isNew = ObservableBoolean()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    fun onSendButton() {
        val tr = allFilldsIGood()
        if (tr) {
            val model = modelTablet.get() ?: ""
            val country = countryTablet.get() ?: ""
            val manufacturer = manufacturer.get() ?: ""
            val productionYear = productionYear.get() ?: ""
            val screenDiagonal = screenDiagonal.get() ?: ""
            val screenResolution = screenResolution.get() ?: ""
            val ram = ram.get() ?: ""
            val cameraResolution = cameraResolution.get() ?: ""
            val seller = seller.get() ?: ""
            val sim = sim.get()
            val isNew = isNew.get()

            Log.d(TAG, "dm-> model =$model, country = $country, sim = $sim")

            getNavigator()?.showToast(model = model, country = country)  // for easier understanding this code can be deleted

            saveTablet(model, country, manufacturer, productionYear, screenDiagonal, screenResolution, ram, sim, cameraResolution, seller, isNew)
        }else{
            getNavigator()?.showToast(model = "Tablet not Save !!", country = "")
        }
    }

    private fun saveTablet(model: String, manufacturerCountry: String, manufacturer: String, productionYear: String, screenDiagonal: String, screenResolution: String, ram: String, sim: Boolean, cameraResolution: String, seller: String, isNew: Boolean) {

        Log.d("LatheViewModel dm->", "- getLatheProperties")
//            _response.value = "Set response Lathe"

        coroutineScope.launch {
            //                var getPropertiesDeferred = LatheApi.retrofitService.getProperties( )
            var getPropertiesDeferred = TabletApi.tabletApiService.saveTablet(TabletBody(model, manufacturer, manufacturerCountry, productionYear, screenDiagonal, screenResolution, ram, sim, cameraResolution, seller, isNew))
            try {
//                    _status.value = LatheApiStatus.LOADING
                Log.d("LatheViewModel", "dm-> all good")
                var listResult = getPropertiesDeferred.await()
//                    _status.value = LatheApiStatus.DONE
//                _properties.value = listResult
            } catch (e: Exception) {
//                _status.value = LatheApiStatus.ERROR
                Log.d("LatheViewModel", "dm-> I think and shure you do not have and internet connection")
//                _properties.value = ArrayList()
            }
        }


    }

    fun allFilldsIGood(): Boolean {
        var isModelFilled = false
        var isCountryFilled = false
        var isManufacturerFilled = false
        var isProductionYearFilled = false
        var isScreenDiagonalFilled = false
        var isScreenResolutionFilled = false
        var isRamFilled = false
        var isCameraResolutionFilled = false
        var isSellerFilled = false

        if (!modelTablet.get().isNullOrEmpty()) isModelFilled = true
        if (!countryTablet.get().isNullOrEmpty()) isCountryFilled = true
        if (!manufacturer.get().isNullOrEmpty()) isManufacturerFilled = true
        if (!productionYear.get().isNullOrEmpty()) isProductionYearFilled = true
        if (!screenDiagonal.get().isNullOrEmpty()) isScreenDiagonalFilled = true
        if (!screenResolution.get().isNullOrEmpty()) isScreenResolutionFilled = true
        if (!ram.get().isNullOrEmpty()) isRamFilled = true
        if (!cameraResolution.get().isNullOrEmpty()) isCameraResolutionFilled = true
        if (!seller.get().isNullOrEmpty()) isSellerFilled = true

        if (!isModelFilled || !isCountryFilled || !isManufacturerFilled || !isProductionYearFilled || !isScreenDiagonalFilled || !isScreenResolutionFilled || !isRamFilled || !isCameraResolutionFilled || !isSellerFilled) {
            getNavigator()?.errorMessages(isModelFilled, isCountryFilled, isManufacturerFilled, isProductionYearFilled, isScreenDiagonalFilled, isScreenResolutionFilled, isRamFilled, isCameraResolutionFilled, isSellerFilled)
            return false
        } else {
            return true
        }

    }

    private var mNavigator: WeakReference<TabletNavigator>? = null  // for easier understanding this code can be deleted

    fun getNavigator() = mNavigator?.get()  // for easier understanding this code can be deleted

    fun setNavigator(navigator: TabletNavigator) {  // for easier understanding this code can be deleted
        clearNavigator()
        mNavigator = WeakReference<TabletNavigator>(navigator)
    }

    private fun clearNavigator() = mNavigator?.clear()  // for easier understanding this code can be deleted
}