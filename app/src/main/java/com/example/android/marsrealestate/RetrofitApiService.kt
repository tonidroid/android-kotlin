package com.example.android.marsrealestate


import com.example.android.marsrealestate.bicycle.Bicycle
import com.example.android.marsrealestate.lathe.Lathe
import com.example.android.marsrealestate.phone.addphone.PhoneBody
import com.example.android.marsrealestate.phone.model.Phone
import com.example.android.marsrealestate.phone.model.PhoneResponse
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


private const val BASE_URL = "http://q11.jvmhost.net/"


private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

object RetrofitApi {

    // 1 For workwith Call
//    private val retrofitCall = Retrofit.Builder()
//            .addConverterFactory(ScalarsConverterFactory.create()) //-  return it as a String // 2 converter factory
//            .baseUrl(BASE_URL)
//            .build()
    var httpClient = OkHttpClient.Builder()
            .build()

    var retrofitCall: Retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(httpClient)
            .build()

    val retrofitApiServiceCall: RetrofitApiServiceForCall by lazy {
        retrofitCall.create(RetrofitApiServiceForCall::class.java)
    }


    // 2 For work with Deferred
    private val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL) // 1 URI
            .addConverterFactory(MoshiConverterFactory.create(moshi)) // 2 converter factory
            .addCallAdapterFactory(CoroutineCallAdapterFactory()) // !! CoroutineCallAdapterFactory allows us returns  Deferred object  .
            .build()

    val retrofitApiService: RetrofitApiService by lazy {
        retrofit.create(RetrofitApiService::class.java)
    }


}

// 1
interface RetrofitApiServiceForCall {

    @GET("vmc_json")
    fun getVmcsString(): Call<String>

//    @POST("vmc")
//    fun saveVmc(@Body vmc: VmcServer): Call<String>

    @POST("phone")
    fun savePhone(@Body phoneBody: PhoneBody): Call<PhoneResponse>
}


// 2
interface RetrofitApiService {

    @GET("lathe_json")
    fun getListLathes(): Deferred<List<Lathe>>
//        fun getProperties(@Query("axes") type: String): Deferred<List<Lathe>> //Call<String>

    // Phone
    @GET("phone_json")
    fun getPhoneList(): Deferred<List<Phone>>

    @POST("phone")
    fun savePhone(@Body phoneBody: PhoneBody): Deferred<PhoneResponse>

    //Bicycle
    @GET("bicycle_json")
    fun getBicycleList(): Deferred<List<Bicycle>>

}


