package com.example.android.marsrealestate.atv

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.AtvItemBinding
import com.example.android.marsrealestate.atv.model.ATV

class AtvRecyclerAdapter(private val onClickListenerAtv: OnClickListenerAtv) : ListAdapter<ATV, AtvRecyclerAdapter.AtvPropertyviewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AtvPropertyviewHolder {
        return AtvPropertyviewHolder(AtvItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: AtvPropertyviewHolder, position: Int) {
        val AtvProperty = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListenerAtv.onClick(AtvProperty)
        }
        holder.bind(AtvProperty)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<ATV>() {
        override fun areItemsTheSame(oldItem: ATV, newItem: ATV): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: ATV, newItem: ATV): Boolean {
            return oldItem.id == newItem.id
        }
    }

    class AtvPropertyviewHolder(private var binding: AtvItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(atv: ATV) {
            binding.property = atv
            binding.executePendingBindings()
        }

    }

    class OnClickListenerAtv(val clickListener: (atv: ATV) -> Unit) {
        fun onClick(atv: ATV) = clickListener(atv)
    }
}