package com.example.android.marsrealestate.bicycle

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class Bicycle (
    val id: Long,
    val model: String,
   //val typeOfProduct: String,
    val manufacturer: String,
    val manufacturerCountry: String,
    val url: String
//add new field
): Parcelable