package com.example.android.marsrealestate.guitar.guitar_add

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentGuitarAddBinding
import com.example.android.marsrealestate.guitar.Guitar

//фрагмент добавления гитары
class GuitarAddFragment : Fragment() {

    private lateinit var binding: FragmentGuitarAddBinding
    private lateinit var newGuitar: Guitar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_guitar_add, container, false)

        //обработчик нажатия на кнопку Добавить
        binding.btnAdd.setOnClickListener {
            binding.apply {
                //создание объекта Гитара
                newGuitar = Guitar()

                try {
                    //заполнение полей объекта
                    newGuitar.apply {
                        model = edtGuitarModel.text.toString()
                        type = edtGuitarType.text.toString()
                        neck = edtGuitarNeck.text.toString()
                        body = edtGuitarBody.text.toString()
                        try {
                            scale = edtGuitarScale.text.toString().toInt()
                        } catch (error: Exception) { //если введено не число
                            scale = 0
                            Log.e("GuitarAddFragment", "error: ${edtGuitarScale.text} is not a number!")
                        }
                        try {
                            fretsNumber = edtGuitarFretsNumber.text.toString().toInt()
                        } catch (error: Exception) {
                            scale = 0
                            Log.e("GuitarAddFragment", "error: ${edtGuitarFretsNumber.text} is not a number!")
                        }
                        color = edtGuitarColor.text.toString()
                        description = edtGuitarDescription.text.toString()

                        //вывод информации на Toast и в Logcat.info
                        Toast.makeText(context, outputInfo(), Toast.LENGTH_LONG).show()
                        Log.i("GuitarAddFragment", outputInfo())
                    }
                } catch (error: Exception) {
                    Log.e("GuitarAddFragment", error.toString())
                }
            }
        }

        return binding.root
    }
}
