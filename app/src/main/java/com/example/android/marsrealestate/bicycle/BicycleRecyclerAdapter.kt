package com.example.android.marsrealestate.bicycle

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.ItemBicycleBinding

class BicycleRecyclerAdapter(private val onClickListenerBicycle: OnClickListenerBicycle):ListAdapter<Bicycle, BicycleRecyclerAdapter.BicycleViewHolder>(DiffCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BicycleViewHolder {
        return BicycleViewHolder(ItemBicycleBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: BicycleViewHolder, position: Int) {
        val press = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListenerBicycle.onClick(press)
        }
        holder.bind(press)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Bicycle>() {
        override fun areItemsTheSame(oldItem: Bicycle, newItem: Bicycle): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Bicycle, newItem: Bicycle): Boolean {
            return oldItem.id == newItem.id
        }
    }


    class BicycleViewHolder(private var binding: ItemBicycleBinding) :
            RecyclerView.ViewHolder(binding.root) {
        fun bind(press: Bicycle) {
            binding.property = press
            binding.executePendingBindings()
        }
    }

    class OnClickListenerBicycle(val clickListener: (bicycle: Bicycle) -> Unit) {
        fun onClick(bicycle: Bicycle) = clickListener(bicycle)
    }


    }

