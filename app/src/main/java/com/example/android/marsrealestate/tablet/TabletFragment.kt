package com.example.android.marsrealestate.tablet

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentTabletBinding

class TabletFragment : Fragment() {
    private val viewModel: TabletViewModel by lazy {
        ViewModelProviders.of(this).get(TabletViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = FragmentTabletBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        binding.lasersGrid.adapter = TabletRecyclerAdapter(TabletRecyclerAdapter.OnClickListenerTablet {
            viewModel.displayOneLathe(it)
            Log.d("LaserListFragment", " dm-> It will redirect to to One Tablet")
        })

        viewModel.navigateToSelectedTablet.observe(this, Observer {
            if ( null != it ) {this.findNavController().navigate(TabletFragmentDirections.actionTabletFragmentToTabletOneFragment(it))
                viewModel.displayOneLatheComplete()
            }
        })

        return binding.root
    }
}