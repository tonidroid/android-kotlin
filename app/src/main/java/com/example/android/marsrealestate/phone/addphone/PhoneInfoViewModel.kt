package com.example.android.marsrealestate.phone.addphone

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel

class PhoneInfoViewModel( app: Application) : AndroidViewModel(app) {

    val isPhoneHas3G = ObservableField<Boolean>(false)
    val isPhoneHas4G = ObservableField<Boolean>(false)

}