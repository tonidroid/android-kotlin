package com.example.android.marsrealestate.tablet.addtablet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentTabletNewBinding

class TabletNewFragment: Fragment(),TabletNavigator {
    lateinit var binding: FragmentTabletNewBinding
    private val viewModel: TabletNewViewModel by lazy {
        ViewModelProviders.of(this).get(TabletNewViewModel::class.java)
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = FragmentTabletNewBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewModel.setNavigator(this) // for easier understanding this code can be deleted


        return binding.root
    }


    override fun showToast(model: String, country: String) {  // for easier understanding this code can be deleted
        Toast.makeText(context, "model: $model, country: $country", Toast.LENGTH_SHORT).show()
    }

    override fun errorMessages(isModelFilled: Boolean, isCountryFilled: Boolean, isManufacturerFilled: Boolean, isProductionYearFilled: Boolean, isScreenDiagonalFilled: Boolean, isScreenResolutionFilled: Boolean, isRamFilled: Boolean, isCameraResolutionFilled: Boolean, isSellerFilled: Boolean) {

        if (!isModelFilled) {
            binding.tabletModel.isErrorEnabled = true
            binding.tabletModel.error = "Enter model"
        } else {
            binding.tabletModel.isErrorEnabled = false
        }

        if (!isCountryFilled) {
            binding.tabletCountry.isErrorEnabled = true
            binding.tabletCountry.error = "Enter country"
        } else {
            binding.tabletCountry.isErrorEnabled = false
        }

        if (!isManufacturerFilled) {
            binding.tabletManufacturer.isErrorEnabled = true
            binding.tabletManufacturer.error = "Enter manufacturer"
        } else {
            binding.tabletManufacturer.isErrorEnabled = false
        }

        if (!isProductionYearFilled) {
            binding.tabletProductionYear.isErrorEnabled = true
            binding.tabletProductionYear.error = "Enter year"
        } else {
            binding.tabletProductionYear.isErrorEnabled = false
        }

        if (!isScreenDiagonalFilled) {
            binding.tabletScreenDiagonal.isErrorEnabled = true
            binding.tabletScreenDiagonal.error = "Enter diagonal"
        } else {
            binding.tabletScreenDiagonal.isErrorEnabled = false
        }

        if (!isScreenResolutionFilled) {
            binding.tabletScreenResolution.isErrorEnabled = true
            binding.tabletScreenResolution.error = "Enter resolution"
        } else {
            binding.tabletScreenResolution.isErrorEnabled = false
        }

        if (!isRamFilled) {
            binding.tabletRam.isErrorEnabled = true
            binding.tabletRam.error = "Enter ram"
        } else {
            binding.tabletRam.isErrorEnabled = false
        }

        if (!isCameraResolutionFilled) {
            binding.tabletCameraResolution.isErrorEnabled = true
            binding.tabletCameraResolution.error = "Enter camera"
        } else {
            binding.tabletCameraResolution.isErrorEnabled = false
        }

        if (!isSellerFilled) {
            binding.tabletSeller.isErrorEnabled = true
            binding.tabletSeller.error = "Enter seller"
        } else {
            binding.tabletSeller.isErrorEnabled = false
        }

    }
}