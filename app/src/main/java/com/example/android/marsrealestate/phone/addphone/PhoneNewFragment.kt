package com.example.android.marsrealestate.phone.addphone

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentPhoneNewBinding


class PhoneNewFragment : Fragment(), PhoneNavigator {


    lateinit var binding: FragmentPhoneNewBinding

    private val viewModel: PhoneNewViewModel by lazy {
        ViewModelProviders.of(this).get(PhoneNewViewModel::class.java)
    }

    // Where we save data about phone
//    private  var phoneDetaileViewModel:  PhoneInfoViewModel by activityViewModels<>()
    private lateinit var phoneInfoViewModel: PhoneInfoViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val application = requireNotNull(activity).application

        binding = FragmentPhoneNewBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewModel.setNavigator(this) // for easier understanding this code can be deleted


        activity?.let {
            phoneInfoViewModel = ViewModelProviders.of(it).get(PhoneInfoViewModel::class.java)

        }

        viewModel.navigateToPhoneCharacteristics.observe(this, Observer {
            if (it) {
                this.findNavController().navigate(
                        PhoneNewFragmentDirections.actionPhoneNewFragmentToPhoneSettingsFragment())
                viewModel.displaySettingComplete()
            }
        })

        return binding.root
    }


    override fun showToast(model: String, country: String) {  // for easier understanding this code can be deleted
        Toast.makeText(context, "model: $model, country: $country", Toast.LENGTH_LONG).show()
    }

    override fun errorMessages(isModelFilled: Boolean, isCountryFilled: Boolean) {

        if (!isModelFilled) {
            binding.phoneModelLayout.isErrorEnabled = true
            binding.phoneModelLayout.error = "Enter model"
        } else {
            binding.phoneModelLayout.isErrorEnabled = false
        }

        if (!isCountryFilled) {
            binding.phoneCountryLayout.isErrorEnabled = true
            binding.phoneCountryLayout.error = "Enter country"
        } else {
            binding.phoneCountryLayout.isErrorEnabled = false
        }

    }


    override fun get3G() = phoneInfoViewModel.isPhoneHas3G
    override fun get4G() = phoneInfoViewModel.isPhoneHas4G

    private var currentSelectedManufacturerIndex = -1

    override fun createSelectManufacturerDialog() {
        val listCategory = listOf("Apple", "Xiomi", "HTC", "Other")

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listCategory.toTypedArray(),
                        currentSelectedManufacturerIndex) { dialog, which ->
                    currentSelectedManufacturerIndex = which
                    viewModel.updateManufacturer(which + 1)
                    dialog.dismiss()
                }
                setTitle("Phone producer")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }


}


