package com.example.android.marsrealestate.tablet.addtablet

interface TabletNavigator{
    fun showToast(model: String, country: String)
    fun errorMessages(isModelFilled: Boolean, isCountryFilled: Boolean, isManufacturerFilled: Boolean, isProductionYearFilled: Boolean, isScreenDiagonalFilled: Boolean, isScreenResolutionFilled: Boolean, isRamFilled: Boolean, isCameraResolutionFilled: Boolean, isSellerFilled: Boolean)
}