package com.example.android.marsrealestate.guitar.guitar_detail

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.android.marsrealestate.guitar.Guitar

class GuitarDetailViewModelFactory(
        private val guitar: Guitar,
        private val app: Application
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GuitarDetailViewModel::class.java)) {
            return GuitarDetailViewModel(guitar, app) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class!")
    }
}