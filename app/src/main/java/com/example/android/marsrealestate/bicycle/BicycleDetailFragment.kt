package com.example.android.marsrealestate.bicycle

import android.app.Application
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.FragmentBicycleDetailBinding
import com.example.android.marsrealestate.overview.OverviewViewModel
import androidx.lifecycle.ViewModelProviders


class BicycleDetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                               savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application
        val binding =  FragmentBicycleDetailBinding.inflate(inflater)

        binding.lifecycleOwner = this


      val bicycle:Bicycle = BicycleDetailFragmentArgs.fromBundle(arguments!!).selectedBicycleProperty

       val viewModelFactory = BicycleDetailViewModelFactory(bicycle, application)
        binding.viewModel = ViewModelProviders.of(
                this, viewModelFactory).get(BicycleDetailViewModel::class.java)

        return binding.root
    }
}


// ---- Model ---

class BicycleDetailViewModel(bicycle: Bicycle, app: Application) : AndroidViewModel(app) {

    private val _selectedProperty = MutableLiveData<Bicycle>()
    val selectedProperty: LiveData<Bicycle>
        get() = _selectedProperty

    init {
        _selectedProperty.value = bicycle
    }

}


class BicycleDetailViewModelFactory(private val bicycle: Bicycle, private val application: Application) :
        ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BicycleDetailViewModel::class.java)) {
            return BicycleDetailViewModel(bicycle, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}