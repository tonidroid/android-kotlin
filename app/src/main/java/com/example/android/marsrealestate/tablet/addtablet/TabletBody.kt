package com.example.android.marsrealestate.tablet.addtablet

class TabletBody (
        var model : String,
        val manufacturer: String,
        val manufacturerCountry: String,
        //  val url: String,
        val productionYear: String,
        val screenDiagonal: String,
        val screenResolution: String,
        val ram: String,
        val hasSim: Boolean,
        val cameraResolution: String,
        val seller: String,
        val productIsNew: Boolean
)