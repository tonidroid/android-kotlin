package com.example.android.marsrealestate.tablet

import com.example.android.marsrealestate.atv.addatv.AtvBody
import com.example.android.marsrealestate.atv.model.ATV
import com.example.android.marsrealestate.tablet.addtablet.TabletBody
import com.example.android.marsrealestate.tablet.model.Tablet
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


private const val BASE_URL = "http://q11.jvmhost.net/"


private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

object TabletApi {

    private val tablet = Retrofit.Builder()
            .baseUrl(BASE_URL) // 1 URI
            .addConverterFactory(MoshiConverterFactory.create(moshi)) // 2 converter factory
            .addCallAdapterFactory(CoroutineCallAdapterFactory()) // !! CoroutineCallAdapterFactory allows us returns  Deferred object  .
            .build()

    val tabletApiService: TabletApiService by lazy {
        tablet.create(TabletApiService::class.java)
    }
}




interface TabletApiService {

    @GET("tablet_json")
    fun getTabletList(): Deferred<List<Tablet>>

    @POST("tablet")
    fun saveTablet(@Body tabletBody: TabletBody): Deferred<String>

    @GET("quadrocycle_json")
    fun getAtvList(): Deferred<List<ATV>>

    @POST("quadrocycle")
    fun saveAtv(@Body atvBody: AtvBody): Deferred<String>
}